-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.6.28-0ubuntu0.15.10.1 - (Ubuntu)
-- Server OS:                    debian-linux-gnu
-- HeidiSQL Version:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table pighunter.auth_assignment
CREATE TABLE IF NOT EXISTS `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`),
  CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table pighunter.auth_assignment: ~1 rows (approximately)
/*!40000 ALTER TABLE `auth_assignment` DISABLE KEYS */;
INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
	('admin', '1', 1457860912),
	('administrator', '1', 1457897718),
	('User', '3', 1457897710),
	('users', '3', 1457897709);
/*!40000 ALTER TABLE `auth_assignment` ENABLE KEYS */;


-- Dumping structure for table pighunter.auth_item
CREATE TABLE IF NOT EXISTS `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`),
  CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table pighunter.auth_item: ~103 rows (approximately)
/*!40000 ALTER TABLE `auth_item` DISABLE KEYS */;
INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
	('/*', 2, NULL, NULL, NULL, 1457860877, 1457860877),
	('/admin/*', 2, NULL, NULL, NULL, 1457860875, 1457860875),
	('/admin/assignment/*', 2, NULL, NULL, NULL, 1457860873, 1457860873),
	('/admin/assignment/assign', 2, NULL, NULL, NULL, 1457860873, 1457860873),
	('/admin/assignment/index', 2, NULL, NULL, NULL, 1457860873, 1457860873),
	('/admin/assignment/view', 2, NULL, NULL, NULL, 1457860873, 1457860873),
	('/admin/default/*', 2, NULL, NULL, NULL, 1457860873, 1457860873),
	('/admin/default/index', 2, NULL, NULL, NULL, 1457860873, 1457860873),
	('/admin/menu/*', 2, NULL, NULL, NULL, 1457860874, 1457860874),
	('/admin/menu/create', 2, NULL, NULL, NULL, 1457860874, 1457860874),
	('/admin/menu/delete', 2, NULL, NULL, NULL, 1457860874, 1457860874),
	('/admin/menu/index', 2, NULL, NULL, NULL, 1457860873, 1457860873),
	('/admin/menu/update', 2, NULL, NULL, NULL, 1457860874, 1457860874),
	('/admin/menu/view', 2, NULL, NULL, NULL, 1457860873, 1457860873),
	('/admin/permission/*', 2, NULL, NULL, NULL, 1457860874, 1457860874),
	('/admin/permission/assign', 2, NULL, NULL, NULL, 1457860874, 1457860874),
	('/admin/permission/create', 2, NULL, NULL, NULL, 1457860874, 1457860874),
	('/admin/permission/delete', 2, NULL, NULL, NULL, 1457860874, 1457860874),
	('/admin/permission/index', 2, NULL, NULL, NULL, 1457860874, 1457860874),
	('/admin/permission/update', 2, NULL, NULL, NULL, 1457860874, 1457860874),
	('/admin/permission/view', 2, NULL, NULL, NULL, 1457860874, 1457860874),
	('/admin/role/*', 2, NULL, NULL, NULL, 1457860874, 1457860874),
	('/admin/role/assign', 2, NULL, NULL, NULL, 1457860874, 1457860874),
	('/admin/role/create', 2, NULL, NULL, NULL, 1457860874, 1457860874),
	('/admin/role/delete', 2, NULL, NULL, NULL, 1457860874, 1457860874),
	('/admin/role/index', 2, NULL, NULL, NULL, 1457860874, 1457860874),
	('/admin/role/update', 2, NULL, NULL, NULL, 1457860874, 1457860874),
	('/admin/role/view', 2, NULL, NULL, NULL, 1457860874, 1457860874),
	('/admin/route/*', 2, NULL, NULL, NULL, 1457860874, 1457860874),
	('/admin/route/assign', 2, NULL, NULL, NULL, 1457860874, 1457860874),
	('/admin/route/create', 2, NULL, NULL, NULL, 1457860874, 1457860874),
	('/admin/route/index', 2, NULL, NULL, NULL, 1457860874, 1457860874),
	('/admin/route/refresh', 2, NULL, NULL, NULL, 1457860874, 1457860874),
	('/admin/rule/*', 2, NULL, NULL, NULL, 1457860874, 1457860874),
	('/admin/rule/create', 2, NULL, NULL, NULL, 1457860874, 1457860874),
	('/admin/rule/delete', 2, NULL, NULL, NULL, 1457860874, 1457860874),
	('/admin/rule/index', 2, NULL, NULL, NULL, 1457860874, 1457860874),
	('/admin/rule/update', 2, NULL, NULL, NULL, 1457860874, 1457860874),
	('/admin/rule/view', 2, NULL, NULL, NULL, 1457860874, 1457860874),
	('/admin/user/*', 2, NULL, NULL, NULL, 1457860875, 1457860875),
	('/admin/user/activate', 2, NULL, NULL, NULL, 1457860875, 1457860875),
	('/admin/user/change-password', 2, NULL, NULL, NULL, 1457860875, 1457860875),
	('/admin/user/delete', 2, NULL, NULL, NULL, 1457860875, 1457860875),
	('/admin/user/index', 2, NULL, NULL, NULL, 1457860875, 1457860875),
	('/admin/user/login', 2, NULL, NULL, NULL, 1457860875, 1457860875),
	('/admin/user/logout', 2, NULL, NULL, NULL, 1457860875, 1457860875),
	('/admin/user/request-password-reset', 2, NULL, NULL, NULL, 1457860875, 1457860875),
	('/admin/user/reset-password', 2, NULL, NULL, NULL, 1457860875, 1457860875),
	('/admin/user/signup', 2, NULL, NULL, NULL, 1457860875, 1457860875),
	('/admin/user/view', 2, NULL, NULL, NULL, 1457860875, 1457860875),
	('/debug/*', 2, NULL, NULL, NULL, 1457860876, 1457860876),
	('/debug/default/*', 2, NULL, NULL, NULL, 1457860876, 1457860876),
	('/debug/default/db-explain', 2, NULL, NULL, NULL, 1457860876, 1457860876),
	('/debug/default/download-mail', 2, NULL, NULL, NULL, 1457860876, 1457860876),
	('/debug/default/index', 2, NULL, NULL, NULL, 1457860876, 1457860876),
	('/debug/default/toolbar', 2, NULL, NULL, NULL, 1457860876, 1457860876),
	('/debug/default/view', 2, NULL, NULL, NULL, 1457860876, 1457860876),
	('/gii/*', 2, NULL, NULL, NULL, 1457860877, 1457860877),
	('/gii/default/*', 2, NULL, NULL, NULL, 1457860877, 1457860877),
	('/gii/default/action', 2, NULL, NULL, NULL, 1457860876, 1457860876),
	('/gii/default/diff', 2, NULL, NULL, NULL, 1457860876, 1457860876),
	('/gii/default/index', 2, NULL, NULL, NULL, 1457860876, 1457860876),
	('/gii/default/preview', 2, NULL, NULL, NULL, 1457860876, 1457860876),
	('/gii/default/view', 2, NULL, NULL, NULL, 1457860876, 1457860876),
	('/gridview/*', 2, NULL, NULL, NULL, 1457860876, 1457860876),
	('/gridview/export/*', 2, NULL, NULL, NULL, 1457860876, 1457860876),
	('/gridview/export/download', 2, NULL, NULL, NULL, 1457860876, 1457860876),
	('/site/*', 2, NULL, NULL, NULL, 1457860877, 1457860877),
	('/site/about', 2, NULL, NULL, NULL, 1457860877, 1457860877),
	('/site/captcha', 2, NULL, NULL, NULL, 1457860877, 1457860877),
	('/site/contact', 2, NULL, NULL, NULL, 1457860877, 1457860877),
	('/site/error', 2, NULL, NULL, NULL, 1457860877, 1457860877),
	('/site/index', 2, NULL, NULL, NULL, 1457860877, 1457860877),
	('/site/login', 2, NULL, NULL, NULL, 1457860877, 1457860877),
	('/site/logout', 2, NULL, NULL, NULL, 1457860877, 1457860877),
	('/user/*', 2, NULL, NULL, NULL, 1457860876, 1457860876),
	('/user/manager/*', 2, NULL, NULL, NULL, 1457860875, 1457860875),
	('/user/manager/bulk-block', 2, NULL, NULL, NULL, 1457860875, 1457860875),
	('/user/manager/bulk-delete', 2, NULL, NULL, NULL, 1457860875, 1457860875),
	('/user/manager/bulk-unblock', 2, NULL, NULL, NULL, 1457860875, 1457860875),
	('/user/manager/create', 2, NULL, NULL, NULL, 1457860875, 1457860875),
	('/user/manager/delete', 2, NULL, NULL, NULL, 1457860875, 1457860875),
	('/user/manager/hand-confirm', 2, NULL, NULL, NULL, 1457860875, 1457860875),
	('/user/manager/index', 2, NULL, NULL, NULL, 1457860875, 1457860875),
	('/user/manager/toggle-block', 2, NULL, NULL, NULL, 1457860875, 1457860875),
	('/user/manager/toggle-superuser', 2, NULL, NULL, NULL, 1457860875, 1457860875),
	('/user/manager/update', 2, NULL, NULL, NULL, 1457860875, 1457860875),
	('/user/manager/view', 2, NULL, NULL, NULL, 1457860875, 1457860875),
	('/user/profile/*', 2, NULL, NULL, NULL, 1457860875, 1457860875),
	('/user/profile/index', 2, NULL, NULL, NULL, 1457860875, 1457860875),
	('/user/profile/password', 2, NULL, NULL, NULL, 1457897597, 1457897597),
	('/user/profile/update', 2, NULL, NULL, NULL, 1457860875, 1457860875),
	('/user/profile/view', 2, NULL, NULL, NULL, 1457860875, 1457860875),
	('/user/security/*', 2, NULL, NULL, NULL, 1457860876, 1457860876),
	('/user/security/change-password', 2, NULL, NULL, NULL, 1457860876, 1457860876),
	('/user/security/confirm', 2, NULL, NULL, NULL, 1457860876, 1457860876),
	('/user/security/login', 2, NULL, NULL, NULL, 1457860875, 1457860875),
	('/user/security/logout', 2, NULL, NULL, NULL, 1457860875, 1457860875),
	('/user/security/recovery', 2, NULL, NULL, NULL, 1457860876, 1457860876),
	('/user/security/register', 2, NULL, NULL, NULL, 1457860876, 1457860876),
	('/user/security/resend', 2, NULL, NULL, NULL, 1457860876, 1457860876),
	('/user/security/reset', 2, NULL, NULL, NULL, 1457860876, 1457860876),
	('admin', 1, NULL, NULL, NULL, 1457860905, 1457860905),
	('administrator', 2, NULL, NULL, NULL, 1457860869, 1457860869),
	('User', 2, NULL, NULL, NULL, 1457897624, 1457897624),
	('users', 1, NULL, NULL, NULL, 1457897696, 1457897696);
/*!40000 ALTER TABLE `auth_item` ENABLE KEYS */;


-- Dumping structure for table pighunter.auth_item_child
CREATE TABLE IF NOT EXISTS `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table pighunter.auth_item_child: ~102 rows (approximately)
/*!40000 ALTER TABLE `auth_item_child` DISABLE KEYS */;
INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
	('administrator', '/*'),
	('administrator', '/admin/*'),
	('administrator', '/admin/assignment/*'),
	('administrator', '/admin/assignment/assign'),
	('administrator', '/admin/assignment/index'),
	('administrator', '/admin/assignment/view'),
	('administrator', '/admin/default/*'),
	('administrator', '/admin/default/index'),
	('administrator', '/admin/menu/*'),
	('administrator', '/admin/menu/create'),
	('administrator', '/admin/menu/delete'),
	('administrator', '/admin/menu/index'),
	('administrator', '/admin/menu/update'),
	('administrator', '/admin/menu/view'),
	('administrator', '/admin/permission/*'),
	('administrator', '/admin/permission/assign'),
	('administrator', '/admin/permission/create'),
	('administrator', '/admin/permission/delete'),
	('administrator', '/admin/permission/index'),
	('administrator', '/admin/permission/update'),
	('administrator', '/admin/permission/view'),
	('administrator', '/admin/role/*'),
	('administrator', '/admin/role/assign'),
	('administrator', '/admin/role/create'),
	('administrator', '/admin/role/delete'),
	('administrator', '/admin/role/index'),
	('administrator', '/admin/role/update'),
	('administrator', '/admin/role/view'),
	('administrator', '/admin/route/*'),
	('administrator', '/admin/route/assign'),
	('administrator', '/admin/route/create'),
	('administrator', '/admin/route/index'),
	('administrator', '/admin/route/refresh'),
	('administrator', '/admin/rule/*'),
	('administrator', '/admin/rule/create'),
	('administrator', '/admin/rule/delete'),
	('administrator', '/admin/rule/index'),
	('administrator', '/admin/rule/update'),
	('administrator', '/admin/rule/view'),
	('administrator', '/admin/user/*'),
	('administrator', '/admin/user/activate'),
	('administrator', '/admin/user/change-password'),
	('administrator', '/admin/user/delete'),
	('administrator', '/admin/user/index'),
	('administrator', '/admin/user/login'),
	('administrator', '/admin/user/logout'),
	('administrator', '/admin/user/request-password-reset'),
	('administrator', '/admin/user/reset-password'),
	('administrator', '/admin/user/signup'),
	('administrator', '/admin/user/view'),
	('administrator', '/debug/*'),
	('administrator', '/debug/default/*'),
	('administrator', '/debug/default/db-explain'),
	('administrator', '/debug/default/download-mail'),
	('administrator', '/debug/default/index'),
	('administrator', '/debug/default/toolbar'),
	('administrator', '/debug/default/view'),
	('administrator', '/gii/*'),
	('administrator', '/gii/default/*'),
	('administrator', '/gii/default/action'),
	('administrator', '/gii/default/diff'),
	('administrator', '/gii/default/index'),
	('administrator', '/gii/default/preview'),
	('administrator', '/gii/default/view'),
	('administrator', '/gridview/*'),
	('administrator', '/gridview/export/*'),
	('administrator', '/gridview/export/download'),
	('administrator', '/site/*'),
	('User', '/site/*'),
	('administrator', '/site/about'),
	('User', '/site/about'),
	('administrator', '/site/captcha'),
	('User', '/site/captcha'),
	('administrator', '/site/contact'),
	('User', '/site/contact'),
	('administrator', '/site/error'),
	('User', '/site/error'),
	('administrator', '/site/index'),
	('User', '/site/index'),
	('administrator', '/site/login'),
	('User', '/site/login'),
	('administrator', '/site/logout'),
	('User', '/site/logout'),
	('administrator', '/user/*'),
	('administrator', '/user/manager/*'),
	('administrator', '/user/manager/bulk-block'),
	('administrator', '/user/manager/bulk-delete'),
	('administrator', '/user/manager/bulk-unblock'),
	('administrator', '/user/manager/create'),
	('administrator', '/user/manager/delete'),
	('administrator', '/user/manager/hand-confirm'),
	('administrator', '/user/manager/index'),
	('administrator', '/user/manager/toggle-block'),
	('administrator', '/user/manager/toggle-superuser'),
	('administrator', '/user/manager/update'),
	('administrator', '/user/manager/view'),
	('administrator', '/user/profile/*'),
	('User', '/user/profile/*'),
	('administrator', '/user/profile/index'),
	('User', '/user/profile/index'),
	('administrator', '/user/profile/password'),
	('User', '/user/profile/password'),
	('administrator', '/user/profile/update'),
	('User', '/user/profile/update'),
	('administrator', '/user/profile/view'),
	('User', '/user/profile/view'),
	('administrator', '/user/security/*'),
	('User', '/user/security/*'),
	('administrator', '/user/security/change-password'),
	('User', '/user/security/change-password'),
	('administrator', '/user/security/confirm'),
	('administrator', '/user/security/login'),
	('User', '/user/security/login'),
	('administrator', '/user/security/logout'),
	('User', '/user/security/logout'),
	('administrator', '/user/security/recovery'),
	('administrator', '/user/security/register'),
	('administrator', '/user/security/resend'),
	('administrator', '/user/security/reset'),
	('admin', 'administrator'),
	('users', 'User');
/*!40000 ALTER TABLE `auth_item_child` ENABLE KEYS */;


-- Dumping structure for table pighunter.auth_rule
CREATE TABLE IF NOT EXISTS `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table pighunter.auth_rule: ~0 rows (approximately)
/*!40000 ALTER TABLE `auth_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_rule` ENABLE KEYS */;


-- Dumping structure for table pighunter.migration
CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table pighunter.migration: ~3 rows (approximately)
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;
INSERT INTO `migration` (`version`, `apply_time`) VALUES
	('m000000_000000_base', 1457821199),
	('m140506_102106_rbac_init', 1457821262),
	('m150703_191015_init', 1457821203);
/*!40000 ALTER TABLE `migration` ENABLE KEYS */;


-- Dumping structure for table pighunter.user_accounts
CREATE TABLE IF NOT EXISTS `user_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `gender` int(11) DEFAULT NULL,
  `birth_place` varchar(255) DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  `pict_url` varchar(255) DEFAULT NULL,
  `address` text,
  `mobile_phone` varchar(13) DEFAULT NULL,
  `password_hash` varchar(255) NOT NULL,
  `auth_key` varchar(255) NOT NULL,
  `administrator` int(11) DEFAULT NULL,
  `creator` int(11) DEFAULT NULL,
  `creator_ip` varchar(40) DEFAULT NULL,
  `confirm_token` varchar(255) DEFAULT NULL,
  `recovery_token` varchar(255) DEFAULT NULL,
  `blocked_at` int(11) DEFAULT NULL,
  `confirmed_at` int(11) DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_unique_login` (`login`),
  UNIQUE KEY `user_unique_username` (`username`),
  UNIQUE KEY `user_unique_phone` (`mobile_phone`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table pighunter.user_accounts: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_accounts` DISABLE KEYS */;
INSERT INTO `user_accounts` (`id`, `login`, `username`, `full_name`, `gender`, `birth_place`, `birth_date`, `pict_url`, `address`, `mobile_phone`, `password_hash`, `auth_key`, `administrator`, `creator`, `creator_ip`, `confirm_token`, `recovery_token`, `blocked_at`, `confirmed_at`, `created_at`, `updated_at`) VALUES
	(1, 'huyadesakai@gmail.com', 'sakai', 'Arif Rahman', 1, 'Baso', '1987-04-01', '1457889973_onepiece.jpg', 'Tower Flamboyan lantai 8 AD Kalibata City', '082112030641', '$2y$13$g6IpWNBu2CdrWGEmPjACRO4.92ELbhrLU1bNQ/cz3PMzgErqj4iTu', '', 1, -2, 'Local', NULL, NULL, NULL, 1457821330, 1457821330, 1457895953),
	(3, 'abc@def.com', 'budi', 'Sakai rock', 1, 'Jakarta', '1987-04-01', '1457897780_10710877_716584445095909_8345829328619481723_n.jpg', 'Jln Tebet timur dalam IV / C no 11', '083804474283', '$2y$13$MkkubV92K5AnQms6EFRuXeZ.PXpiIeL/K59BRPmQEyiLVF5F5vv6i', '', 0, 1, '127.0.0.1', NULL, NULL, NULL, 1457894529, 1457894529, 1457897788);
/*!40000 ALTER TABLE `user_accounts` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
