<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-info">
                <img src='<?php echo Yii::$app->user->identity->avatar;?>' class="img-responsive img-circle img-thumbnail" alt="Responsive image">
            </div>
        </div>
        <div class="user-panel">
            <div class="myinfo">
                <p><?= Yii::$app->user->identity->fullname; ?></p>
            </div>
        </div>

        <!-- search form -->
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Option', 'icon'=>'fa fa-gears', 'options' => ['class' => 'header']],
                    ['label' => 'My Profile', 'icon' => 'fa fa-cog', 'url' => ['/user/profile/index']],
                    ['label' => 'Update Profile', 'icon' => 'fa fa-cog', 'url' => ['/user/profile/update']],
                    ['label' => 'Change Password', 'icon' => 'fa fa-cog', 'url' => ['/user/profile/password']],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                ],
            ]
        ) ?>

    </section>

</aside>
