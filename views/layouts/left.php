<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-info">
                <img src='<?php echo Yii::$app->user->identity->avatar;?>' class="img-responsive img-thumbnail img-circle" alt="Responsive image">
            </div>
        </div>
        <div class="user-panel">
            <div class="myinfo">
                <p><?= Yii::$app->user->identity->fullname; ?></p>
            </div>
        </div>

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Menu', 'options' => ['class' => 'header']],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
              
                    ['label' => 'My Profile', 'icon' => 'fa fa-user', 'url' => ['/profile']],
                ],
            ]
        ) ?>

    </section>

</aside>
